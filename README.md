# book-bar

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.furong.bookbar.mapper.BarMapper">
    <resultMap id="ManagerMap" type="com.furong.bookbar.entity.pojo.Bar">
        <id column="book_id" property="bookId" jdbcType="INTEGER"/>
        <result column="book_name" property="bookName" jdbcType="VARCHAR"/>
        <result column="author" property="author" jdbcType="VARCHAR"/>
        <result column="publish_name" property="publishName" jdbcType="VARCHAR"/>
        <result column="book_price" property="price" jdbcType="INTEGER"/>
        <result column="publish_date" property="publishDate"/>
        <result column="cover_url" property="coverUrl" jdbcType="VARCHAR"/>
        <result column="is_set" property="isSet" jdbcType="TINYINT"/>
        <result column="isbn_no" property="isbnNo" jdbcType="INTEGER"/>
        <result column="desc" property="desc" jdbcType="VARCHAR"/>
    </resultMap>

</mapper>
```

