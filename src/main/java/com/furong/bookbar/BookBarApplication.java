package com.furong.bookbar;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@MapperScan("com.furong.mapper")
public class BookBarApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookBarApplication.class, args);
    }

}
