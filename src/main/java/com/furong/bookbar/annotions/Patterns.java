package com.furong.bookbar.annotions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Patterns {
    /**
     * 指定正则表达式
     *
     * @return
     */
    String pattern() default "";

    /**
     * 校验失败的提示信息
     *
     * @return
     */
    String message() default "字段格式不正确";

}
