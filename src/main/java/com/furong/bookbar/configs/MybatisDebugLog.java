package com.furong.bookbar.configs;

import org.apache.ibatis.logging.stdout.StdOutImpl;

public class MybatisDebugLog extends StdOutImpl {

    public MybatisDebugLog(String clazz) {
        super(clazz);
    }

    @Override
    public boolean isDebugEnabled() {
        return true;
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

}
