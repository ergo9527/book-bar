package com.furong.bookbar.controller;

import com.furong.bookbar.result.Result;
import com.furong.bookbar.service.ActivityService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuxin
 * @date 2021/5/13 21:03
 * @description
 */
@Slf4j
@ResponseBody
@RestController
@Api(description = "activity Api")
@RequestMapping(value = "/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @GetMapping("/{id}")
    public Result findById(@PathVariable("id") long id) {
        return Result.buildSuccess(activityService.findById(id));
    }

    @GetMapping("/all")
    public Result findAll() {
        return Result.buildSuccess(activityService.findAll());
    }
}
