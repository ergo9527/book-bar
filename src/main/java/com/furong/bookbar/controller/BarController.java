package com.furong.bookbar.controller;

import com.furong.bookbar.entity.pojo.Bar;
import com.furong.bookbar.entity.pojo.BookBar;
import com.furong.bookbar.result.Result;
import com.furong.bookbar.service.BarService;
import com.furong.bookbar.service.BookBarService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 16:52
 * @Description :
 */
@Slf4j
@ResponseBody
@RestController
@Api(description = "Bar Api")
@RequestMapping(value = "/bar")
public class BarController {

    @Autowired
    private BarService barService;

    @Autowired
    private BookBarService bookBarService;

    @GetMapping("/findAll")
    public Result findAll() {
        return Result.buildSuccess(barService.findAll());
    }

    @GetMapping("/find/{id}")
    public Result findById(@PathVariable("id") Long id) {
        return Result.buildSuccess(barService.findById(id));
    }

    @PostMapping("/add")
    public Result addOne(@RequestBody Bar bar) {
        barService.insertOne(bar);
        return Result.buildSuccess();
    }

    @PutMapping("/update")
    public Result updateOne(@RequestBody Bar bar) {
        barService.updateOne(bar);
        return Result.buildSuccess();
    }

    @DeleteMapping("/delete/{id}")
    public Result removeById(@PathVariable("id") Long id) {
        barService.deleteOne(id);
        return Result.buildSuccess();
    }

    @PostMapping("/combine")
    public Result combineBook(@RequestBody BookBar bookBar) {
        try {
            bookBarService.combineBook(bookBar);
            return Result.buildSuccess();
        } catch (Exception e) {
            return Result.buildFailure(500, "该书吧已有这本书，请勿重复添加");
        }
    }

    @GetMapping("/find/barCount")
    public Result getBarCount() {
        return Result.buildSuccess(barService.findBarCount());
    }
}
