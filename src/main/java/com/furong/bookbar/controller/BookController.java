package com.furong.bookbar.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.furong.bookbar.entity.pojo.Appoint;
import com.furong.bookbar.entity.pojo.Bar;
import com.furong.bookbar.entity.pojo.Book;
import com.furong.bookbar.entity.vo.AppointVO;
import com.furong.bookbar.entity.vo.TypeCount;
import com.furong.bookbar.exception.CustomerException;
import com.furong.bookbar.mapper.BookMapper;
import com.furong.bookbar.result.MessageCode;
import com.furong.bookbar.result.Result;
import com.furong.bookbar.service.BarService;
import com.furong.bookbar.service.BookService;
import com.sun.org.apache.regexp.internal.RE;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author liuxin
 * @date 2021/4/18 18:29
 * @description
 */
@Slf4j
@ResponseBody
@RestController
@Api(description = "Book Api")
@RequestMapping(value = "/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BarService barService;

    @Autowired
    private BookMapper bookMapper;

    /**
     * 根据id查询详情
     *
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "根据id查询book详情", notes = "根据id查询book详情", httpMethod = "GET")
    public Result findById(@PathVariable("id") Long id) {
        Book book = null;
        try {
            book = bookService.findById(id);
            List<Bar> barList = barService.findBarsByBookId(id);
            book.setBars(barList);
            return Result.buildSuccess(book);
        } catch (Exception e) {
            return Result.buildFailure(MessageCode.PARSE_ERROR);
        }
    }

    /**
     * 预约
     *
     * @param appointVo
     * @return
     */
    @PostMapping("/appoint")
    @ApiOperation(value = "预约", notes = "预约", httpMethod = "POST")
    public Result makeAppoint(@RequestBody AppointVO appointVo) {

        try {
            Appoint appoint = bookService.makeAppoint(appointVo.getBookId(), appointVo.getBarId(), appointVo.getUserId());
            return Result.buildSuccess(appoint);
        } catch (CustomerException e) {
            return Result.buildFailure(e.getMessageCode());
        }
    }

    /**
     * 新增
     *
     * @param book
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增", notes = "新增", httpMethod = "POST")
    public Result addOne(@RequestBody Book book) {
        bookService.insertBookToDB(book);
        return Result.buildSuccess();
    }

    /**
     * 修改
     *
     * @param book
     * @return
     */
    @PutMapping("/update")
    @ApiOperation(value = "修改", notes = "修改", httpMethod = "PUT")
    public Result updateOne(@RequestBody Book book) {
        bookService.updateBook(book);
        return Result.buildSuccess();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除", notes = "删除", httpMethod = "DELETE")
    public Result removeById(@PathVariable("id") Long id) {
        bookService.deleteById(id);
        return Result.buildSuccess();
    }

    /**
     * 分页条件查询
     */
    @ApiOperation("分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "模糊查询关键词", dataType = "String", paramType = "query")
    })
    @GetMapping("/query")
    @ResponseBody
    public Result mutiQueryByPage(@ApiIgnore String keyword) {
        List<Book> books = bookService.mutiQuery(keyword);
        return Result.buildSuccess(books);
    }

    @GetMapping("/find/countsOfType")
    public Result findCountsOfType() {
        JSONObject jsonObject = new JSONObject();
        long bookCount = bookService.selectBookCount();
        List<TypeCount> typeCounts = bookService.findCountsOfType();
        jsonObject.put("total", bookCount);
        jsonObject.put("types", typeCounts);
        return Result.buildSuccess(jsonObject);
    }

    @GetMapping("/find/countsOfBar")
    public Result findCountsOfBar() {
        return Result.buildSuccess(bookService.findCountsOfBar());
    }

    @GetMapping("/find/orderCount")
    public Result findOederCount() {
        return Result.buildSuccess(bookService.findOrderCount());
    }

    @GetMapping("/find/test")
    public Result test() {
        return Result.buildSuccess(bookMapper.test());
    }
}
