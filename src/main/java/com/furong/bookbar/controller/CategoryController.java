package com.furong.bookbar.controller;

import com.furong.bookbar.entity.pojo.Category;
import com.furong.bookbar.result.Result;
import com.furong.bookbar.service.CategoryService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 14:06
 * @Description :
 */
@Slf4j
@ResponseBody
@RestController
@Api(description = "category Api")
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/findAll")
    public Result findAll() {
        return Result.buildSuccess(categoryService.findAll());
    }

    @GetMapping("/find/{id}")
    public Result findById(@PathVariable("id") Long id) {
        return Result.buildSuccess(categoryService.findById(id));
    }

    @PostMapping("/add")
    public Result addOne(@RequestBody Category category) {
        categoryService.insertOne(category);
        return Result.buildSuccess();
    }

    @PutMapping("/update")
    public Result updateOne(@RequestBody Category category) {
        categoryService.updateOne(category);
        return Result.buildSuccess();
    }

    @DeleteMapping("/delete/{id}")
    public Result removeById(@PathVariable("id") Long id) {
        categoryService.deleteOne(id);
        return Result.buildSuccess();
    }
}
