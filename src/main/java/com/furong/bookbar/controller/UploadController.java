package com.furong.bookbar.controller;

import com.furong.bookbar.result.Result;
import com.furong.bookbar.utils.TencentCOS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/upload")
@Slf4j
public class UploadController {

    @PostMapping("/cos")
    public Result ossUpload(@RequestParam("file") MultipartFile file, String folder) throws IOException {

        //获取文件的名称
        String fileName = file.getOriginalFilename();

        //获取文件后缀
        String prefix = fileName.substring(fileName.lastIndexOf("."));

        //使用uuid作为文件名，防止生成的临时文件重复
        File tempFile = File.createTempFile("image-" + UUID.randomUUID().toString().replace("-",""), prefix);

        //将MultipartFile转换成File
        file.transferTo(tempFile);

        //上传文件到腾讯云存储
        //调用腾讯云工具上传文件
        String imageName = TencentCOS.uploadfile(tempFile, folder);

        //程序结束时，删除临时文件
        deleteFile(tempFile);

       log.debug("upload image url is {}",imageName);

       return Result.buildSuccess("\thttps://gxa-study-1304423638.cos.ap-chengdu.myqcloud.com/"+imageName);
    }

    /**
     * 删除临时文件
     * @param files 临时文件，可变参数
     */
    private void deleteFile(File... files) {
        for (File file : files) {
            if (file.exists()) {
                file.delete();
            }
        }
    }
}
