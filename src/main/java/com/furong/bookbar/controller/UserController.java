package com.furong.bookbar.controller;

import com.furong.bookbar.entity.pojo.Appoint;
import com.furong.bookbar.entity.pojo.User;
import com.furong.bookbar.exception.CustomerException;
import com.furong.bookbar.result.MessageCode;
import com.furong.bookbar.result.Result;
import com.furong.bookbar.service.UserService;
import com.furong.bookbar.utils.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.events.Event;

import java.text.ParseException;
import java.util.List;

@Slf4j
@ResponseBody
@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户注册
     *
     * @param user
     * @return
     */
    @PostMapping(value = "/register")
    public Result userRegister(@RequestBody User user) {
        try {
            userService.userRegister(user);
            return Result.buildSuccess();
        } catch (CustomerException e) {
            return Result.buildFailure(e.getMessageCode());
        }
    }

    /**
     * 发送注册验证码短信
     *
     * @param phone
     * @return
     */
    @GetMapping(value = "/send/regisCode")
    public Result sendRegisCode(String phone) {
        try {
            userService.sendRegisterCode(phone);
            return Result.buildSuccess();
        } catch (CustomerException e) {
            return Result.buildFailure(e.getMessageCode());
        }
    }

    @PostMapping("/login")
    public Result userLogin(@RequestBody User user) {
        try {
            User loginUser = userService.userLogin(user.getPhone(), user.getPassword());
            return Result.buildSuccess(loginUser);
        } catch (CustomerException e) {
            return Result.buildFailure(e.getMessageCode());
        }
    }

    @GetMapping("/get/{userId}")
    public Result findById(@PathVariable("userId") Long userId) {
        User user = userService.findById(userId);
        return Result.buildSuccess(user);
    }

    @PostMapping("/update")
    public Result updateUserInfo(@RequestBody User user) {
        userService.updateUser(user);
        return Result.buildSuccess();
    }

    @GetMapping("/appoint/find/{userId}")
    public Result findAppointByUid(@PathVariable("userId") Long userId) {
        try {
            List<Appoint> list = userService.findAppointByUserId(userId);
            return Result.buildSuccess(list);
        } catch (ParseException e) {
            return Result.buildFailure(MessageCode.PARSE_ERROR);
        }
    }

    @GetMapping("/appoint/remove/{appId}")
    public Result removeAppointById(@PathVariable("appId") Long appId) {
        userService.removeAppointById(appId);
        return Result.buildSuccess();
    }

    @GetMapping("/find/userCount")
    public Result getUserCount() {
        return Result.buildSuccess(userService.findUserCount());
    }
}
