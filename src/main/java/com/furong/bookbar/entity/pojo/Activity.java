package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author liuxin
 * @date 2021/5/13 21:05
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "activity")
public class Activity {

    private Long id;

    private String title;

    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    @TableField(exist = false)
    private List<ActivityPic> pics;
}
