package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuxin
 * @date 2021/5/13 21:13
 * @description
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "activity_pic")
public class ActivityPic {

    private Long id;

    private String url;

    private Long activityId;
}
