package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author : liuxin
 * @Date: 2021/5/5 9:27
 * @Description : 预约图书信息
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "appoint")
public class Appoint {


    @TableId(value = "appoint_id", type = IdType.AUTO)
    private Long appointId;

    @TableField(value = "book_id")
    private Long bookId;

    @TableField(value = "bar_id")
    private Long barId;

    @TableField(value = "user_id")
    private Long userId;

    @TableField(value = "created_at")
    private String createdAt;

    @TableField(value = "deadline")
    private String deadline;

    /**
     * 非数据库字段
     */

    @TableField(exist = false)
    private Book book;

    @TableField(exist = false)
    private Bar bar;

    /**
     * 0-预约已过期
     * 1-预约生效中
     */
    @TableField(exist = false)
    private Integer status;

}
