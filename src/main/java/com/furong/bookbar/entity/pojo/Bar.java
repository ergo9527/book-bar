package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Liuxin
 * @date 2021/4/23 13:42
 * @description 类的描述
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "bar")
public class Bar {

    @TableId(value = "bar_id", type = IdType.AUTO)
    private Long barId;

    @TableField("bar_name")
    private String barName;

    @TableField("location")
    private String location;

    @TableField("cover")
    private String cover;
}
