package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "book")
public class Book implements Serializable {

    private static final long serialVersionUID = 6023151057966210867L;

    @TableId(value = "book_id", type = IdType.AUTO)
    private Long bookId;

    @TableField(value = "book_name")
    private String name;

    @TableField(value = "author")
    private String author;

    @TableField(value = "publish_name")
    private String publishName;

    @TableField(value = "book_price")
    private Double price;

    @TableField(value = "publish_date")
    private String publishDate;

    @TableField(value = "category_id")
    private Long categoryId;

    @TableField(value = "cover_url")
    private String coverUrl;

    @TableField(value = "is_set")
    private Boolean isSet;

    @TableField(value = "isbn_no")
    private Long isbnNo;

    @TableField(value = "description")
    private String description;

    @TableField(exist = false)
    private String type;

    @TableField(exist = false)
    private List<String> pics;

    @TableField(exist = false)
    private List<Bar> bars;

}
