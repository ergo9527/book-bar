package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : liuxin
 * @Date: 2021/5/5 10:53
 * @Description : .
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "book_bar")
public class BookBar {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(hidden = true)
    private Long id;

    @TableField(value = "book_id")
    private Long bookId;

    @TableField(value = "bar_id")
    private Long barId;
}
