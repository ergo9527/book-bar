package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 13:58
 * @Description :
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "category")
public class Category {

    @TableId(value = "category_id", type = IdType.AUTO)
    private Long id;

    private String categoryName;
}
