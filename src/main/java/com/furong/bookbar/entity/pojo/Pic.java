package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Liuxin
 * @date 2021/4/23 14:25
 * @description 类的描述
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "pics")
public class Pic {

    @TableId(value = "pic_id", type = IdType.AUTO)
    private Long picId;

    @TableField("pic_url")
    private String picUrl;
}
