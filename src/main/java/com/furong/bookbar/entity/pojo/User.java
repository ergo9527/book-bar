package com.furong.bookbar.entity.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.furong.bookbar.annotions.Patterns;
import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName(value = "user")
public class User implements Serializable {

    private static final long serialVersionUID = -3529880321538933920L;

    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 手机号码作为用户名
     */
    @Patterns(pattern = "^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$", message = "手机号码格式不正确")
    private String phone;

    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 用户性别 1-男 0-女
     */
    private int gender;

    private String birthday;

    private String password;

    private String salt;

    @TableField(value = "user_created_date")
    private String userCreatedDate;

    @TableField(value = "last_login_date")
    private String lastLoginDate;

    @TableField(value = "id_card")
    private String idCard;

    @TableField(value = "image_url")
    private String imageUrl;

    /**
     * 注册验证码,不对应数据库字段
     */
    @TableField(exist = false)
    private String regisCode;

    @TableField(value = "user_type")
    private int userType;

}
