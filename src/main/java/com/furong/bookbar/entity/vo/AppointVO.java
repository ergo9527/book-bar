package com.furong.bookbar.entity.vo;

import lombok.Data;

/**
 * @Author : liuxin
 * @Date: 2021/5/5 9:42
 * @Description : 封装预约的各种id
 */
@Data
public class AppointVO {

    private Long userId;
    private Long bookId;
    private Long barId;

}
