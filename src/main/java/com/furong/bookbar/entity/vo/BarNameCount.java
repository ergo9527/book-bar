package com.furong.bookbar.entity.vo;

import lombok.Data;

/**
 * @author liuxin
 * @date 2021/6/1 19:13
 * @description
 */
@Data
public class BarNameCount {

    private String barName;
    private int count;
}
