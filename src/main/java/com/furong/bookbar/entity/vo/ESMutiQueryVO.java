package com.furong.bookbar.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ESMutiQueryVO {

    private String queryString;

    private Integer page;


}
