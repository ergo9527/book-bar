package com.furong.bookbar.entity.vo;

import lombok.Data;

/**
 * @author liuxin
 * @date 2021/6/1 19:04
 * @description
 */
@Data
public class TypeCount {

    private String categoryName;

    private int count;
}
