package com.furong.bookbar.exception;

import com.furong.bookbar.result.MessageCode;
import lombok.Data;

/**
 * @author Liuxin
 * @date 2021/3/25 14:18
 * @description 公共异常处理
 */
@Data
public class CustomerException extends Exception {

    private MessageCode messageCode;

    public CustomerException(MessageCode messageCode) {
        //给父类传递异常信息
        super(messageCode.getMessage());
        this.messageCode = messageCode;
    }
}
