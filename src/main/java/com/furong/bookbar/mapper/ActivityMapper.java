package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Activity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liuxin
 * @date 2021/5/13 21:08
 * @description
 */

@Mapper
@Component
public interface ActivityMapper extends BaseMapper<Activity> {

}
