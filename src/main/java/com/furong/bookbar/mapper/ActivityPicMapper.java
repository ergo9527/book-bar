package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.ActivityPic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author liuxin
 * @date 2021/5/13 21:12
 * @description
 */

@Mapper
@Component
public interface ActivityPicMapper extends BaseMapper<ActivityPic> {
}
