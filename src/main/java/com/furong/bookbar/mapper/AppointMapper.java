package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Appoint;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/5/5 9:52
 * @Description : .
 */
@Mapper
@Repository
public interface AppointMapper extends BaseMapper<Appoint> {
}
