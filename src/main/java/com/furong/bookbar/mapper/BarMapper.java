package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Bar;
import com.furong.bookbar.entity.pojo.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/4/23 16:11
 * @description 类的描述
 */
@Mapper
@Repository
public interface BarMapper extends BaseMapper<Bar> {

    @Select("select a.* from bar a,book_bar b where a.bar_id=b.bar_id and b.book_id=#{id}")
    List<Bar> findByBookId(Long id);
}
