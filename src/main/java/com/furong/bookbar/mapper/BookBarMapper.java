package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.BookBar;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * @Author : liuxin
 * @Date: 2021/5/5 10:52
 * @Description : .
 */
@Mapper
@Repository
public interface BookBarMapper extends BaseMapper<BookBar> {
}
