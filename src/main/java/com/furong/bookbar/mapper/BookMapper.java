package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Book;
import com.furong.bookbar.entity.vo.BarNameCount;
import com.furong.bookbar.entity.vo.TypeCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author liuxin
 * @date 2021/4/18 15:07
 * @description
 */
@Mapper
@Repository
public interface BookMapper extends BaseMapper<Book> {

    //    @Select("SELECT b.*,c.category_name as type from book b,category c WHERE CONCAT(b.book_name,b.author,c.category_name) like '%${keyword}%'")
    List<Book> mutiQuery(String keyword);

    @Select("SELECT c.category_name,count(*) as count from book b,category c WHERE b.category_id=c.category_id GROUP BY b.category_id")
    List<TypeCount> findCountsOfType();

    @Select("SELECT bar.bar_name,count(*) as count from bar,book_bar where bar.bar_id=book_bar.bar_id GROUP by book_bar.bar_id")
    List<BarNameCount> findCountsOfBar();

    Book test();


}
