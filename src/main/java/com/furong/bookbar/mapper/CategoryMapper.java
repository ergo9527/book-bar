package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Category;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 14:08
 * @Description :
 */
@Mapper
@Repository
public interface CategoryMapper extends BaseMapper<Category> {
}
