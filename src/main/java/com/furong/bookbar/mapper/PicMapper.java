package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.Pic;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/4/23 13:59
 * @description 类的描述
 */
@Mapper
@Repository
public interface PicMapper extends BaseMapper<Pic> {

}
