package com.furong.bookbar.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.furong.bookbar.entity.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Liuxin
 * @date 2021/4/17 13:58
 * @description 类的描述
 */
@Mapper
@Repository
public interface UserMapper extends BaseMapper<User> {
}
