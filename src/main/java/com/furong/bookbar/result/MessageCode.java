package com.furong.bookbar.result;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageCode {

    EXCEPTION(49999, "预期之外的异常!"),
    ERROR(50000, "对不起,系统开小差了!"),
    LOGIN_CODE_INVALID(50001, "验证码失效,请重试!"),
    LOGIN_CODE_INCORRECT(50002, "验证码错误,请重试!"),
    CODE_SENT_FAILURE(50003, "验证码发送失败!"),
    PHONE_CAN_NOT_BE_EMPTY(50004, "手机号码不能为空!"),
    PHONE_WRONG(50005, "手机号码格式错误!"),
    USER_EXIST(50006, "用户名已存在!"),
    PWD_CAN_NOT_BE_EMPTY(50007, "密码不能为空!"),
    USERNAME_OR_PWD_WRONG(50008, "用户名或密码错误!"),
    PARSE_ERROR(50009, "类型转换异常");


    //错误编码
    private Integer code;

    //错误信息
    private String message;
}
