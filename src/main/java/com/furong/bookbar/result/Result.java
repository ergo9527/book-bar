package com.furong.bookbar.result;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 5405305738382874615L;

    /**
     * 错误码
     */
    private int code;

    /**
     * 错误信息
     */
    private String msg;

    /**
     * 响应数据
     */
    private T data;

    private Result(MessageCode messageCode) {
        this(messageCode.getCode(), messageCode.getMessage());
    }

    private Result(MessageCode messageCode, T data) {
        this(messageCode.getCode(), messageCode.getMessage(), data);
    }

    private Result(int code, String msg) {
        this(code, msg, null);
    }

    private Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 构建成功的返回(without data)
     */
    public static <T> Result<T> buildSuccess() {
        return new Result(0, "success~");
    }

    /**
     * 构建成功的返回(with data)
     */
    public static <T> Result<T> buildSuccess(@Nullable T data) {
        return new Result(0, "success~", data);
    }

    /**
     * 构建失败的返回
     */
    public static <T> Result<T> buildFailure(MessageCode messageCode) {
        return new Result(messageCode);
    }

    /**
     * 构建失败的返回
     */
    public static <T> Result<T> buildFailure(int code, String msg) {
        return new Result(code, msg);
    }
}
