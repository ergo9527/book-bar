package com.furong.bookbar.service;

import com.furong.bookbar.entity.pojo.Activity;

import java.util.List;

/**
 * @author liuxin
 * @date 2021/5/13 21:04
 * @description
 */

public interface ActivityService {

    Activity findById(Long id);

    List<Activity> findAll();

}
