package com.furong.bookbar.service;

import com.furong.bookbar.entity.pojo.Bar;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/4/25 10:20
 * @description 类的描述
 */
public interface BarService {

    List<Bar> findBarsByBookId(Long id);

    /**
     * 增
     *
     * @param bar
     */
    void insertOne(Bar bar);

    /**
     * 删
     *
     * @param id
     */
    void deleteOne(Long id);

    /**
     * 改
     *
     * @param bar
     */
    void updateOne(Bar bar);

    /**
     * 根据id查
     *
     * @param id
     * @return
     */
    Bar findById(Long id);

    /**
     * 查询全部
     *
     * @return
     */
    List<Bar> findAll();

    long findBarCount();
}
