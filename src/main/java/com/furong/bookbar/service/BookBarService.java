package com.furong.bookbar.service;

import com.furong.bookbar.entity.pojo.BookBar;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 17:11
 * @Description :
 */
public interface BookBarService {

    void combineBook(BookBar bookBar);
}
