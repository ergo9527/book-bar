package com.furong.bookbar.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.furong.bookbar.entity.pojo.*;
import com.furong.bookbar.entity.vo.BarNameCount;
import com.furong.bookbar.entity.vo.ESMutiQueryVO;
import com.furong.bookbar.entity.vo.TypeCount;
import com.furong.bookbar.exception.CustomerException;

import java.text.ParseException;
import java.util.List;

/**
 * @author liuxin
 * @date 2021/4/18 15:07
 * @description
 */
public interface BookService {

    Book findById(Long id) throws CustomerException;

    List<Pic> findBookPics(Long bookId);

    Appoint makeAppoint(Long bookId, Long barId, Long userId) throws CustomerException;

    void updateBook(Book book);

    void deleteById(Long id);

    void insertBookToDB(Book book);

    List<Book> mutiQuery(String keyword);

    List<TypeCount> findCountsOfType();

    List<BarNameCount> findCountsOfBar();

    long selectBookCount();

    long findOrderCount();

}
