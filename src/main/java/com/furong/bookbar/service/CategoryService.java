package com.furong.bookbar.service;

import com.furong.bookbar.entity.pojo.Category;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 14:09
 * @Description :
 */
public interface CategoryService {

    /**
     * 增
     *
     * @param category
     */
    void insertOne(Category category);

    /**
     * 删
     *
     * @param id
     */
    void deleteOne(Long id);

    /**
     * 改
     *
     * @param category
     */
    void updateOne(Category category);

    /**
     * 根据id查
     *
     * @param id
     * @return
     */
    Category findById(Long id);

    /**
     * 查询全部
     *
     * @return
     */
    List<Category> findAll();
}
