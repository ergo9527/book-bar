package com.furong.bookbar.service;

import com.furong.bookbar.entity.pojo.Appoint;
import com.furong.bookbar.entity.pojo.User;
import com.furong.bookbar.exception.CustomerException;

import java.text.ParseException;
import java.util.List;

public interface UserService {

    User findByPhone(String phone) throws CustomerException;

    void userRegister(User user) throws CustomerException;

    void sendRegisterCode(String phone) throws CustomerException;

    User userLogin(String phone, String pwd) throws CustomerException;

    User findById(Long id);

    User updateUser(User user);

    List<Appoint> findAppointByUserId(Long id) throws ParseException;

    void removeAppointById(Long id);

    long findUserCount();
}
