package com.furong.bookbar.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.furong.bookbar.entity.pojo.Activity;
import com.furong.bookbar.entity.pojo.ActivityPic;
import com.furong.bookbar.mapper.ActivityMapper;
import com.furong.bookbar.mapper.ActivityPicMapper;
import com.furong.bookbar.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liuxin
 * @date 2021/5/13 21:04
 * @description
 */

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityPicMapper acMapper;

    public Activity findById(Long id) {
        QueryWrapper<ActivityPic> wrapper = new QueryWrapper<>();
        wrapper.eq("activity_id", id);
        List<ActivityPic> pics = acMapper.selectList(wrapper);

        Activity activity = activityMapper.selectById(id);
        activity.setPics(pics);

        return activity;
    }

    public List<Activity> findAll() {

        List<Activity> activities = activityMapper.selectList(null);

        for (Activity activity : activities) {
            QueryWrapper<ActivityPic> wrapper = new QueryWrapper<>();
            wrapper.eq("activity_id", activity.getId());
            List<ActivityPic> pics = acMapper.selectList(wrapper);
            activity.setPics(pics);
        }

        return activities;
    }


}
