package com.furong.bookbar.service.impl;

import com.furong.bookbar.entity.pojo.Bar;
import com.furong.bookbar.mapper.BarMapper;
import com.furong.bookbar.service.BarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/4/25 10:33
 * @description 类的描述
 */
@Slf4j
@Service
public class BarServiceImpl implements BarService {

    @Autowired
    private BarMapper barMapper;

    @Override
    public List<Bar> findBarsByBookId(Long id) {
        return barMapper.findByBookId(id);
    }

    @Override
    public void insertOne(Bar bar) {
        barMapper.insert(bar);
    }

    @Override
    public void deleteOne(Long id) {

    }

    @Override
    public void updateOne(Bar bar) {
        barMapper.updateById(bar);
    }

    @Override
    public Bar findById(Long id) {
        return barMapper.selectById(id);
    }

    @Override
    public List<Bar> findAll() {
        return barMapper.selectList(null);
    }

    @Override
    public long findBarCount() {
        return barMapper.selectCount(null);
    }
}
