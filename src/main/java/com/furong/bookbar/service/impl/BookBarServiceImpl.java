package com.furong.bookbar.service.impl;

import com.furong.bookbar.entity.pojo.BookBar;
import com.furong.bookbar.mapper.BookBarMapper;
import com.furong.bookbar.service.BookBarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 17:12
 * @Description :
 */
@Service
public class BookBarServiceImpl implements BookBarService {

    @Autowired
    private BookBarMapper bookBarMapper;

    @Override
    public void combineBook(BookBar bookBar) {
        bookBarMapper.insert(bookBar);
    }
}
