package com.furong.bookbar.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.furong.bookbar.entity.pojo.*;
import com.furong.bookbar.entity.vo.BarNameCount;
import com.furong.bookbar.entity.vo.TypeCount;
import com.furong.bookbar.exception.CustomerException;
import com.furong.bookbar.mapper.*;
import com.furong.bookbar.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author liuxin
 * @date 2021/4/18 15:08
 * @description
 */
@Slf4j
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private PicMapper picMapper;

    @Autowired
    private BarMapper barMapper;

    @Autowired
    private AppointMapper appointMapper;

    @Autowired
    private BookBarMapper bookBarMapper;


    private static HashMap<Long, String> bookTypeMap;

    private static String getBookType(Long categoryId) {
        bookTypeMap = new HashMap<>();
        bookTypeMap.put(1L, "科幻");
        bookTypeMap.put(2L, "悬疑/推理");
        bookTypeMap.put(3L, "文学");
        return bookTypeMap.get(categoryId);
    }

    /**
     * 根据id查询图书详情
     *
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public Book findById(Long id) {
        Book book = bookMapper.selectById(id);
        List<Pic> pics = findBookPics(id);
        List<String> picUrls = new ArrayList<>();
        for (Pic pic : pics) {
            picUrls.add(pic.getPicUrl());
        }
        book.setPics(picUrls);
        book.setType(getBookType(book.getCategoryId()));
        return book;
    }

    @Override
    public List<Pic> findBookPics(Long bookId) {
        //根据手机号查询用户
        QueryWrapper<Pic> wrapper = new QueryWrapper<>();
        wrapper.eq("book_id", bookId);
        return picMapper.selectList(wrapper);
    }

    @Override
    public Appoint makeAppoint(Long bookId, Long barId, Long userId) throws CustomerException {

        //构建预约信息
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Book book = findById(bookId);
        Bar bar = barMapper.selectById(barId);

        Date createdAt = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(createdAt);
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        Date deadline = calendar.getTime();

        Appoint appoint = Appoint.builder()
                .bookId(bookId)
                .barId(barId)
                .userId(userId)
                .createdAt(sdf.format(createdAt))
                .deadline(sdf.format(deadline))
                .book(book)
                .bar(bar)
                .status(1)
                .build();

        appointMapper.insert(appoint);


        return appoint;
    }

    @Override
    public void updateBook(Book book) {
        bookMapper.updateById(book);
    }

    @Override
    public void deleteById(Long id) {
        bookMapper.deleteById(id);
    }

    @Override
    public void insertBookToDB(Book book) {
        bookMapper.insert(book);
    }

    @Override
    public List<Book> mutiQuery(String keyword) {
        List<Book> books = bookMapper.mutiQuery(keyword);
//        if (keyword != null && keyword != "") {
//            QueryWrapper<Book> wrapper = new QueryWrapper<>();
//            wrapper.like("book_name", keyword)
//                    .or().like("author",keyword)
//                    .or().like()
//        }

        return books;
    }

    @Override
    public List<TypeCount> findCountsOfType() {
        return bookMapper.findCountsOfType();
    }

    @Override
    public List<BarNameCount> findCountsOfBar() {
        return bookMapper.findCountsOfBar();
    }

    @Override
    public long selectBookCount() {
        return bookMapper.selectCount(null);
    }

    @Override
    public long findOrderCount() {
        return appointMapper.selectCount(null);
    }
}
