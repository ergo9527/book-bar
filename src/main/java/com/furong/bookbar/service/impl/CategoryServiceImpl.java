package com.furong.bookbar.service.impl;

import com.furong.bookbar.entity.pojo.Category;
import com.furong.bookbar.mapper.CategoryMapper;
import com.furong.bookbar.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author : liuxin
 * @Date: 2021/5/28 14:13
 * @Description :
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;


    @Override
    public void insertOne(Category category) {
        categoryMapper.insert(category);
    }

    @Override
    public void deleteOne(Long id) {
        categoryMapper.deleteById(id);
    }

    @Override
    public void updateOne(Category category) {
        categoryMapper.updateById(category);
    }

    @Override
    public Category findById(Long id) {
        return categoryMapper.selectById(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryMapper.selectList(null);
    }
}
