package com.furong.bookbar.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.furong.bookbar.entity.pojo.Appoint;
import com.furong.bookbar.entity.pojo.Bar;
import com.furong.bookbar.entity.pojo.Book;
import com.furong.bookbar.entity.pojo.User;
import com.furong.bookbar.exception.CustomerException;
import com.furong.bookbar.mapper.AppointMapper;
import com.furong.bookbar.mapper.BarMapper;
import com.furong.bookbar.mapper.BookMapper;
import com.furong.bookbar.mapper.UserMapper;
import com.furong.bookbar.result.MessageCode;
import com.furong.bookbar.service.UserService;
import com.furong.bookbar.utils.Md5Utils;
import com.furong.bookbar.utils.RedisCache;
import com.furong.bookbar.utils.SendSmsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AppointMapper appointMapper;

    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private BarMapper barMapper;

    @Autowired
    private RedisCache redisCache;

    private static final String REGISTER_CODE_HEAD = "register_code";

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public User findByPhone(String phone) throws CustomerException {

        //判断手机号是否为空
        if (StringUtils.isEmpty(phone)) {
            throw new CustomerException(MessageCode.PHONE_CAN_NOT_BE_EMPTY);
        }
        //根据手机号查询用户
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.eq("phone", phone);
        return userMapper.selectOne(userWrapper);
    }

    @Override
    public void userRegister(User user) throws CustomerException {
        //判空
        if (StringUtils.isEmpty(user.getPhone())) {
            throw new CustomerException(MessageCode.PHONE_CAN_NOT_BE_EMPTY);
        } else if (!user.getPhone().matches("^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$")) {
            throw new CustomerException(MessageCode.PHONE_WRONG);
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            throw new CustomerException(MessageCode.PWD_CAN_NOT_BE_EMPTY);
        }

        //1.判断验证码是否正确
        String regisCode = redisCache.getCacheHash(REGISTER_CODE_HEAD, user.getPhone());
        if (StringUtils.isEmpty(regisCode)) {   //验证码失效
            throw new CustomerException(MessageCode.LOGIN_CODE_INVALID);
        } else if (!regisCode.equals(user.getRegisCode())) {    //验证码错误
            throw new CustomerException(MessageCode.LOGIN_CODE_INCORRECT);
        }

        //2.根据手机号查找用户是否重复注册
        User u = findByPhone(user.getPhone());
        if (u != null) {    //用户已存在
            throw new CustomerException(MessageCode.USER_EXIST);
        }

        //3.验证码正确&用户不存在---注册
        //生成salt
        String salt = Md5Utils.createSalt();
        user.setSalt(salt.toString());
        //password加密
        String encodePwd = Md5Utils.md5Password(user.getPassword(), salt);
        user.setPassword(encodePwd);
        //设置创建时间
        user.setUserCreatedDate(sdf.format(new Date()));
        //添加新用户
        userMapper.insert(user);
    }

    @Override
    public void sendRegisterCode(String phone) throws CustomerException {
        //手机号为空或格式不正确
        if (StringUtils.isEmpty(phone) ||
                !phone.matches("^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\\d{8}$")) {
            throw new CustomerException(MessageCode.PHONE_WRONG);
        }
        //生成随机验证码
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append(new Random().nextInt(10));
        }
        String verCode = sb.toString();
        log.info(verCode);
        //发送验证码
        SendSmsUtils.sendSms(phone, verCode);
        //将验证码储存在Redis中，有效时间30min
        redisCache.setCacheHash(REGISTER_CODE_HEAD, phone, verCode);
    }

    @Override
    public User userLogin(String phone, String pwd) throws CustomerException {
        User user = findByPhone(phone);
        if (user == null) {     //用户不存在
            throw new CustomerException(MessageCode.USERNAME_OR_PWD_WRONG);
        }
        //比对密码是否正确
        String salt = user.getSalt();
        String password = user.getPassword();
        String encodePwd = Md5Utils.md5Password(pwd, salt);
        if (!password.equals(encodePwd)) {
            //密码错误
            throw new CustomerException(MessageCode.USERNAME_OR_PWD_WRONG);
        }
        //密码正确,屏蔽密码和盐并更新最近登录时间
        user.setSalt("");
        user.setPassword("");
        user.setLastLoginDate(sdf.format(new Date()));
        return user;
    }

    @Override
    public User findById(Long id) {
        User user = userMapper.selectById(id);
        return user;
    }

    @Override
    public User updateUser(User user) {

        //密码不为空则需要修改，重新加密
        if (!StringUtils.isEmpty(user.getPassword())) {
            String salt = Md5Utils.createSalt();
            String newPwd = Md5Utils.md5Password(user.getPassword(), salt);
            user.setPassword(newPwd);
            user.setSalt(salt);
        }
        userMapper.updateById(user);
        return userMapper.selectById(user.getUserId());
    }

    @Override
    public List<Appoint> findAppointByUserId(Long id) throws ParseException {
        QueryWrapper<Appoint> wrapper = new QueryWrapper();
        wrapper.eq("user_id", id);
        List<Appoint> list = appointMapper.selectList(wrapper);
        for (Appoint appoint : list) {
            Book book = bookMapper.selectById(appoint.getBookId());
            Bar bar = barMapper.selectById(appoint.getBarId());
            appoint.setBar(bar);
            appoint.setBook(book);
            Date deadline = sdf.parse(appoint.getDeadline());
            Date nowDate = new Date();
            if (deadline.before(nowDate)) {
                appoint.setStatus(0);
            } else {
                appoint.setStatus(1);
            }
        }
        return list;
    }

    @Override
    public void removeAppointById(Long id) {
        appointMapper.deleteById(id);
    }

    @Override
    public long findUserCount() {
        return userMapper.selectCount(null);
    }
}
