package com.furong.bookbar.utils;

import org.springframework.stereotype.Component;
import sun.misc.BASE64Encoder;

import java.security.MessageDigest;

@Component
public class Md5Utils {

    private static final String SALT_MODEL = "abcdefghijklmnopqrstuvwxyz1234567890";

    /**
     * 加密的方法
     *
     * @param password
     * @param salt
     * @return
     */
    public static String md5Password(String password, String salt) {

        try {

            // 1.获取MD5加密算法
            MessageDigest messageDigest = MessageDigest.getInstance("md5");

            // 单向加密 只能加密不能解密
            byte[] md5 = messageDigest.digest((password + salt).getBytes());

            // 2.BASE64Encoder进行编码
            BASE64Encoder encoder = new BASE64Encoder();

            return encoder.encode(md5);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    /**
     * 获取盐
     *
     * @return
     */
    public static String createSalt() {

        //生成六位字符salt
        StringBuffer salt = new StringBuffer();
        char[] m = SALT_MODEL.toCharArray();
        for (int i = 0; i < 12; i++) {
            char c = m[(int) (Math.random() * 36)];
            salt = salt.append(c);
        }
        return salt.toString();
    }


}